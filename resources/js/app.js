import {createApp} from 'vue'

import Antd from 'ant-design-vue';
import App from './app.vue'

import 'ant-design-vue/dist/reset.css';

import router from "./router";
const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(Antd);
app.mount('#app');
