import useAuthStore from "@/stores/auth";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
    {
        name: "home",
        path: "/",
        component: () => import("./pages/home.vue"),
        meta: {
            requiresAuth: true,
        },
    },
    {
        name: "login",
        path: "/login",
        component: () => import("./pages/login.vue")
    },
    {
        path: "/:catchAll(.*)",
        component: () => import("./pages/404.vue"),
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        const authStore = useAuthStore();
        if (authStore.token) {
            next();
        } else {
            next({ name: "login" });
        }
    } else {
        next();
    }
});

export default router;
