<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     */
    public function show(Organization $organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Organization $organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Organization $organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Organization $organization)
    {

    }

    /**
     * Remove the specified resource from storage.
     */
    public function send(Request $request)
    {
        $data = $request->only(['path', 'data', 'method']);
        $ebarimtServiceUrl = env('VITE_EBARIMT_SERVICE_URL');
        $client = new Client();
        $path = $data['path'];

        $headers = [
            'Content-Type' => 'application/json',
        ];

        $requestMethod = 'GET';

        if (isset($data['data'])) {
            $requestMethod = 'POST';
        }
        if (isset($data['method'])) {
            $requestMethod = $data['method'];
        }

        $requestUrl = "$ebarimtServiceUrl/$path";
        $responseBody = null;
        try {
            $response = $client->request($requestMethod, $requestUrl, [
                'headers' => $headers,
                'json' => isset($data['data']) ? $data['data'] : null,
            ]);
            $responseBody = $response->getBody()->getContents();

            $timestamp = now()->format('Y-m-d H:i');
            Log::info("[$timestamp] Request to $requestUrl was successful. Response: $responseBody");
        } catch (\Exception $e) {
            $timestamp = now()->format('Y-m-d H:i');
            $payloadError = isset($data['data']) ? ['data' => $data['data']] : null;
            $plainText = $payloadError ? str_replace(['{', '}', '"', ':'], ['[', ']', '', ' =>'], json_encode($payloadError)) : "";
            Log::error("[$timestamp] Request to $requestUrl failed. Error: {" . strval($e->getMessage()) . "}. PAYLOAD: " . $plainText);
            $responseBody = $e->getMessage();
        }
        return empty($responseBody) ? true : $responseBody;
    }
}
