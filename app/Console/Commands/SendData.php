<?php

namespace App\Console\Commands;

use App\Models\Organization;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send POST request to /api/send';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $registers = Organization::pluck('register');
        $flaskUrl = env('VITE_EBARIMT_SERVICE_URL');
        foreach ($registers as $register) {
            $response = Http::get("$flaskUrl/sendData?register=$register");
	    $timestamp = now()->format('Y-m-d H:i');
            if ($response->successful()) {
                Log::info("[$timestamp] SendData ажиллаа: $register.");
            } else {
                Log::error("[$timestamp] SendData алдаа гарлаа:  $register.");
            }
        }
    }
}
